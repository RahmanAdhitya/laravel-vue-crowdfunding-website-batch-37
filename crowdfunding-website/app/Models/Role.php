<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\str;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    use HasFactory;
    

    protected $fillable = [ 'name' ];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $increment=false;


    protected static function boot() {

        parent:: boot();

        static::creating(function ($model) {
            if ( ! $model->getKey()) {
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
        });
    }
   
}
